<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield('title')</title>

<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

@yield('head-css')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Styles -->
<style>
	html, body {
	    background-color: #fff;
	    color: #636b6f;
	    font-family: 'Nunito', sans-serif;
	    font-weight: 200;
	    height: 100vh;
	    margin: 0;
	}

	.container {
		width: 100%;
		height: 100vh;
	}

	.site-header {
		background-color: #999;
		color: #000;
		font-size: 20px;
		height: 8%;
		position: top;
		top: 0;
	}

	.top-right {
		position: relative;
	    right: 10px;
	    top: 18px;
	}

	.content {
		height: 92%;
	}

	.row {
		content: "";
		display: flex;
		clear: both;
		height: 100%;
	}

	.col-12 {
		float: left;
		width: 100%;
	}

	.col-6 {
		border: outset;
		border-color: #999;
		border-width: 1px;
		float: left;
		padding: 10px;
		width: 50%;
		overflow-x: scroll;
		overflow-y: scroll;
	}

	.title {
	    font-size: 84px;
	}

	.links > a {
	    color: #000;
	    padding: 0 25px;
	    font-size: 20px;
	    font-weight: 900;
	    letter-spacing: .1rem;
	    text-decoration: none;
	    text-transform: uppercase;
	}

	.m-b-md {
	    margin-bottom: 30px;
	}
</style>

@yield('head-scripts')