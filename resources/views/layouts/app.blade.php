<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
    	@include('components.head')
    </head>

    <body>
    	<div class="container">
    		@include('components.header')

    		<div class="content">
    			@yield('main-content')
    		</div>

	    	@include('components.footer')
    	</div>
    </body>
</html>