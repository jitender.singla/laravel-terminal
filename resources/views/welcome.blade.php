@extends('layouts.app')

@section('title', 'PHP Terminal App')

@section('main-content')

    <div class="row">
        <div class="col-6" style="background-color: #000; color: #fff;">
            <p>
                <span style="width: 5%;">$&nbsp;</span>
                <span id="terminal" contenteditable="true" style="width: 95%; float: right;">click here to type command...</span>
            </p>
        </div>

        <div class="col-6" id="terminal-output" style="background-color: #ddd;">
            $ php -v <br />
            @switch(gettype($output))
                @case('array')
                    @foreach($output as $line)
                        {{ $line }}<br />
                    @endforeach
                @break

                @case('string')
                    <pre>{{ $output }}</pre>
                @break

                @case('default')
                    @php print($output) @endphp
                @break
            @endswitch
        </div>
    </div>
@endsection

@section('footer-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            fix_onChange_editable_elements();
        });

        function fix_onChange_editable_elements()
        {
            var commandLine = document.getElementById('terminal');

            commandLine.onkeydown = function(event) {
                var elem = this;
                if (event.keyCode == 13) {
                    var command = this.innerHTML;
                    $.ajax({
                        url: "execute-query?command="+command,
                        type: 'GET',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Cache-Control': 'no-cache, no-store, must-revalidate', 
                            'Pragma': 'no-cache', 
                            'Expires': '0'
                        }
                    }).done(function(res) {
                        if (res.status == "success") {
                            elem.innerHTML = 'click here to type command...';
                            document.activeElement.blur();
                            
                            document.getElementById('terminal-output').innerHTML = res.output;
                        } else {
                            elem.innerHTML = 'click here to type command...';
                            document.activeElement.blur();
                            
                            document.getElementById('terminal-output').innerHTML = '$ '+command+'<br />Could not execute command...';
                        }
                    }).fail(function() {
                        elem.innerHTML = 'click here to type command...';
                        document.activeElement.blur();
                        
                        document.getElementById('terminal-output').innerHTML = '$ '+command+'<br />Could not execute command...';
                    }); 
                }
            }

            commandLine.onfocus = function() {
                this.innerHTML = '';
            };

            commandLine.onblur = function() {
                // if (this.innerHTML != this.data_orig)
                //     this.onchange();

                // delete this.data_orig;
                // do nothing
            };
        }
    </script>
@endsection