<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
	protected $validCommands = [
		'ls',
		'cp',
		'cal',
		'cat',
		'mkdir',
		'touch',
		'ps',
		'php',
		'whoami'
	];

	public function index(Request $request) {
		exec('php -v', $output);
		return view('welcome', compact('output'));
	}

	public function executeQuery(Request $request)
	{
		try {
			$outputString = '$ '.$request->command.'<br />';

			if (!in_array(explode(' ', $request->command)[0], $this->validCommands))
				return response()->json(['status' => 'success', 'output' => $outputString.'Command not found!'], 200);

			ob_start();
			exec($request->command." 2>&1", $output);
			ob_end_clean();

			if (!empty($output)) {
				switch (gettype($output)) {
					case 'string':
						$output = '<pre>'.$output.'</pre>';
						break;

					case 'array':
						if ($request->command == 'ls')
							$output = '<pre>'.implode(" ", $output).'</pre>';
						else
							$output = '<pre>'.implode('<br />', $output).'</pre>';
						break;
					
					default:
						$output = 'Output type: `'.gettype($output).'` not supported';
						break;
				}
			} else {
				$output = 'Command Executed...';
			}

			$outputString .= $output;

			return response()->json(['status' => 'success', 'output' => $outputString], 200);
		} catch (\Exception $e) {
			return response()->json(['status' => 'success', 'output' => $outputString.$e->getMessage()], 200);
		}
	}
}
