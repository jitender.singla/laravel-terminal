# laravel-terminal

## Getting Started

Follow below for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them. Bellow instructions are given for CentOS 7. You may use similar steps with different command for respective different Operating Systems.

#### MYSQL

CentOS 7 prefers MariaDB, a fork of MySQL managed by the original MySQL developers and designed as a replacement for MySQL. If you run yum install mysql on CentOS 7, it is MariaDB that is installed rather than MySQL.

```
 $ sudo yum -y install wget
 $ wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
 $ sudo rpm -ivh mysql57-community-release-el7-9.noarch.rpm
 $ sudo yum -y install mysql-server
 $ sudo systemctl start mysqld
```

During the installation process, a temporary password is generated for the MySQL root user. Locate it in the mysqld.log with this command:

```
 $ sudo grep 'temporary password' /var/log/mysqld.log
 $ mysql_secure_installation
```

You'll receive feedback on the strength of your new password, and then you'll be immediately prompted to change it again. Since you just did, you can confidently say No.
After we decline the prompt to change the password again, we'll press Y and then ENTER to all the subsequent questions.

#### PHP 7.2

Add the Remi CentOS repository as CentOS ships with old versions of PHP.

```
 $ sudo rpm -Uvh http://rpms.remirepo.net/enterprise/remi-release-7.rpm
 $ sudo yum -y install yum-utils
 $ sudo yum -y update
```

Install PHP 7.1 now

```
 $ sudo yum-config-manager --enable remi-php72
 $ sudo yum -y install php php-opcache
```

required php extensions
mbstring, php-mongodb, xml, mysql, mcrypt, gd, zip

#### Composer

Follow below instructions for installing composer.
```
 $ sudo yum -y update
 $ cd /tmp
 $ sudo curl -sS https://getcomposer.org/installer | php
 $ sudo mv composer.phar /usr/local/bin/composer
```

### Project Setup

Take clone of project in projects root directory. Switch to project directory and follow below steps.

```
 $ sudo chmod -R 777 storage/
 $ sudo chmod -R 777 bootstrap/cache
 $ cp .env.example .env
 $ composer install
```

Set production environment in .env (production or dev). Fill database details in .env file. And follow instructions

```
 $ php artisan key:generate
```